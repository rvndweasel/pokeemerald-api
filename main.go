package main

import (
	"fmt"
	"pokeemerald-api/data/logic"
	"pokeemerald-api/utils"
)

func saveToJSON() {
	levelUpMovesPath, eggMovesPath, teachableMovesPath := utils.LoadPaths()

	pokemons, err := logic.ParseAndMergeData(levelUpMovesPath, eggMovesPath, teachableMovesPath)
	if err != nil {
		fmt.Println("Error:", err)
		return
	}

	if err := utils.SavePokemonData(pokemons, "output.json"); err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("Data saved to output.json")
}

func returnSpecificPokemon() {
	pokemons, err := logic.LoadPokemonsFromJSON("output.json")
	if err != nil {
		fmt.Println("Error:", err)
		return
	}
	pokemonName := utils.PokemonNameInput()
	logic.FindAndPrintPokemon(pokemonName, pokemons)
}

func main() {
	saveToJSON()
}

// POKEEMERALD DIRECTORY
//   - /home/rvndweasel/Documents/projects/personal_rom_hack/pokeemerald-expansion/
