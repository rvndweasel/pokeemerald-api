package utils

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func PokemonNameInput() string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Enter a pokemon name: ")

	pokemonName, err := reader.ReadString('\n')
	if err != nil {
		fmt.Printf("Error reading input: %v\n", err)
	}

	pokemonName = CorrectPokemonName(pokemonName)
	return pokemonName
}

func FilePathInput() string {
	reader := bufio.NewReader(os.Stdin)
	fmt.Println("Enter filepath to pokeemerald directory:")
	pokeemeraldPath, err := reader.ReadString('\n')

	if err != nil {
		fmt.Printf("Error reading input: %v\n", err)
	}
	pokeemeraldPath = strings.TrimSpace(pokeemeraldPath)

	lastChar := pokeemeraldPath[len(pokeemeraldPath)-1:]
	if lastChar != "/" {
		pokeemeraldPath = pokeemeraldPath + "/"
	}

	return pokeemeraldPath
}
