package utils

import (
	"regexp"
	"strings"
)

func CorrectPokemonName(name string) string {
	updateChars := strings.NewReplacer("é", "e", "♂", " male ", "♀", " female ", "_", " - ")
	name = updateChars.Replace(name)

	nonAlphaRegex := regexp.MustCompile(`[^a-zA-Z ]+`)
	name = nonAlphaRegex.ReplaceAllString(name, "")

	splitName := strings.Fields(name)
	if len(splitName) > 1 {
		name = strings.Join(splitName, "-")
	}

	name = strings.ToLower(name)

	return name
}

func ProcessPokemonName(rawName string) string {
	// Find uppercase beginnings of name parts
	nameParts := regexp.MustCompile("[A-Z][^A-Z]*").FindAllString(rawName, -1)

	// Convert each part to lowercase
	for i, part := range nameParts {
		nameParts[i] = strings.ToLower(part)
	}

	// Join the parts with a dash
	return strings.Join(nameParts, "-")
}
