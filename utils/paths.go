package utils

import (
	"path/filepath"
)

func LoadPaths() (string, string, string) {
	basePath := "/home/rvndweasel/Documents/projects/personal_rom_hack/pokeemerald-expansion/"
	levelUpMovesPath := filepath.Join(basePath, FilePaths["levelUpMovesPath"])
	eggMovesPath := filepath.Join(basePath, FilePaths["eggMovesPath"])
	teachableMovesPath := filepath.Join(basePath, FilePaths["teachableMovesPath"])

	return levelUpMovesPath, eggMovesPath, teachableMovesPath
}
