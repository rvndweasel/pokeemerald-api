package utils

import "strconv"

func ConvertToInt(s string) int {
	val, _ := strconv.Atoi(s)
	return val
}

func ConvertToString(i int) string {
	val := strconv.Itoa(i)
	return val
}
