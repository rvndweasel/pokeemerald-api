package utils

import (
	"encoding/json"
	"fmt"
	"os"
	"pokeemerald-api/data/models"
)

var FilePaths = map[string]string{
	"speciesPath":        "src/data/pokemon/species_info.h",
	"eggMovesPath":       "src/data/pokemon/egg_moves.h",
	"teachableMovesPath": "src/data/pokemon/teachable_learnsets.h",
	"levelUpMovesPath":   "src/data/pokemon/level_up_learnsets.h",
	"evolutionPath":      "src/data/pokemon/evolution.h",
	"battleMovesPath":    "src/data/battle_moves.h",
	"itemsPath":          "src/data/items.h",
	"wildEncountersPath": "src/data/wild_encounters.h",
}

func CheckfilesExist(pokeemeraldPath string) {
	_, err := os.Stat(pokeemeraldPath)
	if os.IsNotExist(err) {
		fmt.Printf("pokeemerald path does not exist:\n\t%v\n", pokeemeraldPath)
	} else {
		for _, value := range FilePaths {
			filePath := pokeemeraldPath + value
			_, err := os.Stat(filePath)
			if os.IsNotExist(err) {
				fmt.Printf("file path does not exist:\n\t%v\n", filePath)
			}
		}
	}
}

func SaveToFile(filename string, data string) error {
	file, err := os.Create(filename)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.WriteString(data)
	return err
}

func SavePokemonData(pokemons []models.Pokemon, outputFilePath string) error {
	jsonOutput, err := json.MarshalIndent(pokemons, "", "  ")
	if err != nil {
		return fmt.Errorf("failed to marshal to JSON: %v", err)
	}

	if err := SaveToFile(outputFilePath, string(jsonOutput)); err != nil {
		return fmt.Errorf("failed to save JSON to file: %v", err)
	}

	return nil
}
