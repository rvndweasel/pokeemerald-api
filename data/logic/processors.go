package logic

import "pokeemerald-api/data/models"

func ParseAndMergeData(levelUpMovesPath, eggMovesPath, teachableMovesPath string) ([]models.Pokemon, error) {
	levelUpPokemons, err := ParseLevelUpMovesToJSON(levelUpMovesPath)
	if err != nil {
		return nil, err
	}

	eggMovesMap, err := ParseEggMovesToJSON(eggMovesPath)
	if err != nil {
		return nil, err
	}

	teachableMovesMap, err := ParseTeachableMovesToJSON(teachableMovesPath)
	if err != nil {
		return nil, err
	}

	for i, pokemon := range levelUpPokemons {
		if moves, exists := eggMovesMap[pokemon.Name]; exists {
			levelUpPokemons[i].EggMoves = moves
		}

		// Merge teachable moves
		if moves, exists := teachableMovesMap[pokemon.Name]; exists {
			levelUpPokemons[i].TeachableMoves = moves // Assuming you have TeachableMoves field in the Pokemon model
		}
	}

	return levelUpPokemons, nil
}
