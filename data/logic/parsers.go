package logic

import (
	"fmt"
	"os"
	"pokeemerald-api/data/models"
	"pokeemerald-api/utils"
	"regexp"
	"strings"
)

func ParseEggMovesToJSON(filePath string) (map[string][]string, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	content := string(data)

	pokemonRegex := regexp.MustCompile(`egg_moves\((\w+),`)
	moveRegex := regexp.MustCompile(`MOVE_(\w+)`)

	pokemonMatches := pokemonRegex.FindAllStringSubmatchIndex(content, -1)

	eggMovesMap := make(map[string][]string)

	for _, match := range pokemonMatches {
		pokemonName := utils.CorrectPokemonName(content[match[2]:match[3]])

		startPos := match[1]
		endPos := match[1]

		if len(pokemonMatches) > 1 {
			nextMatchStart := pokemonMatches[1][0]
			tempEndPos := strings.LastIndex(content[startPos:nextMatchStart], ")")
			if tempEndPos == -1 {
				return nil, fmt.Errorf("could not determine endPos for %s", pokemonName)
			}
			endPos = startPos + tempEndPos
		} else {
			tempEndPos := strings.LastIndex(content[startPos:], ")")
			if tempEndPos == -1 {
				return nil, fmt.Errorf("could not determine endPos for %s", pokemonName)
			}
			endPos = startPos + tempEndPos
		}

		if startPos > endPos {
			return nil, fmt.Errorf("startPos is greater than endPos for %s", pokemonName)
		}

		movesBlock := content[startPos:endPos]

		moveMatches := moveRegex.FindAllStringSubmatch(movesBlock, -1)
		for _, moveMatch := range moveMatches {
			eggMovesMap[pokemonName] = append(eggMovesMap[pokemonName], moveMatch[1])
		}

		if len(pokemonMatches) > 1 {
			pokemonMatches = pokemonMatches[1:]
		}
	}

	return eggMovesMap, nil
}

func ParseLevelUpMovesToJSON(filePath string) ([]models.Pokemon, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	nameRegex := regexp.MustCompile(`^static const struct LevelUpMove s([A-Z][a-zA-Z]+)LevelUpLearnset\[\] = \{`)
	moveRegex := regexp.MustCompile(`LEVEL_UP_MOVE\(\s*(\d+), MOVE_([A-Z_]+)\),`)

	lines := strings.Split(string(data), "\n")

	var pokemons []models.Pokemon

	for i := 0; i < len(lines); i++ {
		if match := nameRegex.FindStringSubmatch(lines[i]); match != nil {
			rawPokemonName := match[1]
			processedPokemonName := utils.ProcessPokemonName(rawPokemonName)
			pokemon := models.Pokemon{
				Name: processedPokemonName,
			}
			i++
			for moveRegex.MatchString(lines[i]) {
				moveMatches := moveRegex.FindStringSubmatch(lines[i])
				level := moveMatches[1]
				moveName := strings.ToLower(strings.Replace(moveMatches[2], "_", "-", -1))
				move := models.Move{
					Level: utils.ConvertToInt(level),
					Name:  moveName,
				}
				pokemon.Moves = append(pokemon.Moves, move)
				i++
			}
			pokemons = append(pokemons, pokemon)
		}
	}

	return pokemons, nil
}

func ParseTeachableMovesToJSON(filePath string) (map[string][]string, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	content := string(data)

	// Regex to identify each Pokemon section
	sectionRegex := regexp.MustCompile(`s([A-Z][a-zA-Z]+)TeachableLearnset\[\] = \{([\s\S]*?)\};`)
	// Regex to extract each move
	moveRegex := regexp.MustCompile(`MOVE_([A-Z_]+),`)

	sections := sectionRegex.FindAllStringSubmatch(content, -1)

	teachableMovesMap := make(map[string][]string)

	for _, section := range sections {
		pokemonName := strings.ToLower(section[1])
		movesBlock := section[2]

		moveMatches := moveRegex.FindAllStringSubmatch(movesBlock, -1)
		for _, moveMatch := range moveMatches {
			moveName := strings.ToLower(strings.Replace(moveMatch[1], "_", "-", -1))
			if moveName != "unavailable" {
				teachableMovesMap[pokemonName] = append(teachableMovesMap[pokemonName], moveName)
			}
		}
	}

	return teachableMovesMap, nil
}
