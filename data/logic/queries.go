package logic

import (
	"fmt"
	"pokeemerald-api/data/models"
	"strings"
)

func FindAndPrintPokemon(pokemonName string, pokemons []models.Pokemon) {
	for _, p := range pokemons {
		if strings.EqualFold(p.Name, pokemonName) { // Case-insensitive comparison
			fmt.Printf("Name: %s\n", p.Name)

			// Print Level Up Moves
			fmt.Println("Level Up Moves:")
			for _, move := range p.Moves {
				fmt.Printf("  Level %d: %s\n", move.Level, move.Name)
			}

			// Print Egg Moves
			fmt.Println("Egg Moves:")
			for _, eggMove := range p.EggMoves {
				fmt.Printf("  %s\n", eggMove)
			}
			return
		}
	}

	fmt.Println("Pokemon not found!")
}
