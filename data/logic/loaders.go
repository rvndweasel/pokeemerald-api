package logic

import (
	"encoding/json"
	"os"
	"pokeemerald-api/data/models"
)

func LoadPokemonsFromJSON(filePath string) ([]models.Pokemon, error) {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	var pokemons []models.Pokemon
	err = json.Unmarshal(data, &pokemons)
	if err != nil {
		return nil, err
	}

	return pokemons, nil
}
