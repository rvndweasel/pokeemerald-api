package models

type Move struct {
	Level int    `json:"level"`
	Name  string `json:"name"`
}

type Pokemon struct {
	Name           string   `json:"name"`
	Moves          []Move   `json:"moves"`
	EggMoves       []string `json:"egg_moves"`
	TeachableMoves []string `json:"teachable_moves"`
}
