# Pokémon Emerald Data API
This project aims to parse the data from the "pokeemerald" source files and create a personal API. While there are several APIs available for the official Pokémon games, this project fills the gap for customized versions of Pokémon Emerald, allowing users to access and update data from their unique romhacks.

## Motivation
The "pokeemerald" project is a decompilation of Pokémon Emerald. With its source code, users can recompile and create their own version of Pokémon Emerald. Some have gone as far as adding Pokémon, items, and features from newer generations into this Gen 3 game. This project provides an API for such customized versions, giving access to updated data.

## Features
- Parse "pokeemerald" source files to extract Pokémon data.
- Store the data in a structured JSON format.
- Correct and format Pokémon names to make them API-friendly.
- Check for the existence of necessary files before processing.

## Future Features
- Expand parsing to include items, abilities, evolutions, and more.
- API endpoints to access and modify the parsed data.
- User interface for easy interaction with the API.

## Setup and Installation

1. Clone this repository:
```
git clone https://gitlab.com/rvndweasel/pokeemerald-api
```

2. Navigate to the project directory:
```
cd pokeemerald-api
```

3. Run the main program:
```
go run main.go
```

## Usage
1. Input the path to your "pokeemerald" directory when prompted.
2. The program will check for the existence of necessary files.
3. Data will be parsed and saved to `output.json`.

## License
This project is free software: you can redistribute it and/or modify it under the terms of the [License-name] License.